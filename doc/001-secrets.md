# Secrets handling

## Topics covered

- Nature of secrets. Plain text as universal interface.
- Problems with secrets (bootstrapping, all security is always derived from some secrets).
- Good passwords - https://xkcd.com/936/
- Importance of local workstation disk encryption.
- Importance of 2FA.
- Environment variables are everywhere.
- Hint: space before a command to not to write to .history file in bash if [HISTCONTROL](https://www.gnu.org/software/bash/manual/html_node/Bash-Variables.html#index-HISTCONTROL) properly set.
- Encrypting secrets - e.g. aws kms, google kms secret, etc.
- Encryption at rest and in transit (tls)
- Best practices
  - Fetch on run-time
  - [Follow principle of least privileges](https://en.wikipedia.org/wiki/Principle_of_least_privilege)
  - Security is a never-ending *process*; should be architected in layers, ideally following [zero-trust](https://en.wikipedia.org/wiki/Zero_trust_security_model) approach
  - Pay attention to access policies (limit scope of permissions, no ADMIN to everyone)

## Useful tools

- [pass](https://www.passwordstore.org/) - Unix/Linux; gpg encrypted file
- [keepassxc](https://keepassxc.org/) - multi-platform, local
- [Bitwarden](https://bitwarden.com/) (multi-platform, online)
- LastPass (multi-platform, online)
- 1Password (multi-platform, online)
- Hardware tokens - Yubikey (also as password storage with Password Safe)
- [Pre-commit](https://pre-commit.com/) framework with [GitLeak](https://github.com/gitleaks/gitleaks)
- [Hashicorp vault](https://www.vaultproject.io/)

## Demo time

1. Example1: Sourcing secrets in bulk from file, sub-process calls to pass binary

   Having below gpg encrypted secrets in pass:

   ```bash
   pass ls demo

   demo
   ├── secret-1
   └── secret-2
   ```

   One can prepare a file with variable declarations:

   ```bash
   export SECRET_1=$(pass demo/secret-1)
   export SECRET_2=$(pass demo/secret-2)
   # call to GCP cloud secret manager
   export SECRET_3=$(gcloud secrets versions access latest --secret="SOME_SECRET_NAME" --project="SOME_GCP_PROJECT")
   ```
   That can be sourced with `source my_vars.sh` (file extension does not matter).

   ```bash
   # Now variable value is present in the environment:

   echo $SECRET_1

   # Result: secret-1-value
   ```

1. Example2: Testing if variable is not empty

   ```bash
   variable_1=$(gcloud secrets versions access latest --secret="SOME_SECRET_NAME" --project="SOME_GCP_PROJECT_NAME")

   [ -z $variable_1 ] && "Empty" || echo "Not empty"

   # result: Not empty
   ```

1. Example3: Setup `pre-commit-hook` with `gitleaks` plugin:

   Demo Gods were not in favor this evening :(
   I should have [RTFM](https://en.wikipedia.org/wiki/RTFM) before the demo.

1. Example4: Creating good random secrets from cli (mac, Linux). For windows see your password manager capabilities.

   ```bash
   pwgen -s 20 1
   # result: 20 character-long string - lcTJjjny17DiKJxaSDJ3
   #
   # see pwgen man page: man pwgen
   ```

1. Example5: Accessing secrets values in terraform code

   ```bash
   data "google_secret_manager_secret_version" "cloudflare-api-token" {
     secret = "cf-api-token"
   }

   provider "cloudflare" {
     api_token = data.google_secret_manager_secret_version.cloudflare-api-token.secret_data
   }
   ```

## Takeaways

- Make sure you use 2FA for online service if possible.
- Use password managers; choose strong master password.
- Strong master password is a looooooong password.
- Do not commit secrets in plain text to git repository.
- Do not send secrets in plain text.
- Fetch secrets from secret manager and inject them at runtime (read from the environment, mount as file if necessary, etc).
- Setup pre-commit + gitleaks.
- Use secret detection jobs in CI/CD pipelines.
- Review you setup periodically.
- Rotate secrets periodically.

# DestinationIT 2023 sessions

1. Secrets handling - [2023-11-30 Thu] - [link](./doc/001-secrets.md), [recording](https://drive.google.com/file/d/1Ust3fSF06KxsWJKCnajgN8midG3lrw6Z/view?usp=drive_link)

1. DevOps 101 - [2023-11-29 Wed] - [recording](https://drive.google.com/file/d/113suFdDcgSb3DTk5uu39GYtHD26jPnHf/view?usp=drive_link)
